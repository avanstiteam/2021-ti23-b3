/**
 * Standard c includes needed for this module
 * */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

/**
 * include selfmade header files
 * */
#include "clocksync.h"
#include "AOC.h"
#include "lcd_module.h"
#include "play_recording_module.h"
#include "HK_radio.h"
#include "volume_module.h"
#include "licht_orgel_module.h"

/**
 * include freeRTOS libraries
 * */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "sdkconfig.h"
#include "esp32/rom/uart.h"
#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "board.h"

/**
 * include downloaded libraries
 * */
#include "smbus.h"
#include "i2c-lcd1602.h"
#include "mcp23017.h"

#if __has_include("esp_idf_version.h")
#include "esp_idf_version.h"
#else
#define ESP_IDF_VERSION_VAL(major, minor, patch) 1
#endif

#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 1, 0))
#include "esp_netif.h"
#else
#include "tcpip_adapter.h"
#endif

#define TAG "MAIN"
#define LYRAT_SDA_PIN 18
#define LYRAT_SCL_PIN 23

#undef USE_STDIN

#define I2C_MASTER_NUM I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN 0 // disabled
#define I2C_MASTER_RX_BUF_LEN 0 // disabled
#define I2C_MASTER_FREQ_HZ 100000
#define I2C_MASTER_SDA_IO 18
#define I2C_MASTER_SCL_IO 23
#define LCD_NUM_ROWS 4
#define LCD_NUM_COLUMNS 40
#define LCD_NUM_VIS_COLUMNS 20

mcp23017_t mcp23017;
TimerHandle_t timer_1_sec;

static uint8_t last_button_state = 0; //variable to store the last pressed button

int current_recording = 0; //variable to store the current selected recording

int increase_amount_volume = 1; //variable to store the current increase amount of the volume
int current_volume = 50; //variable to store the current volume

int current_radio_index = 0; //variable to store the index of the selected radio
int radio_is_running = 0; //variable to store the state of the radio

int timer_set_minutes = 0; //variable to store the total minutes of the timer set by the user
int timer_set_seconds = 0; //variable to store the total seconds of the timer set by the user
int minutes_remaining = 0; //variable to store the amount of minutes remaining when timer is started
int seconds_remaining = 0; //variable to store the amount of seconds remaining when timer is started
int timer_enabled = 0; //variable to store the state of the timer --> running / not running
int timer_finished = 1; //variable to store the state of the timer --> finished / nog finished

int stopwatch_enabled = 0; //variable to store the state of the stopwatch --> running / not running
int stopwatch_seconds = 0; //variable to store the current amount of seconds
int stopwatch_minutes = 0; //variable to store the current amount of minutes

int alarm_enabled = 0; //variable to store the state of the alarm --> running / not running
int alarm_hours = 0; //variable to store the hour of the alarm
int alarm_minutes = 0; //variable to store the minute of the alarm

int light_organ_enabled = 0; //variable to store the state of the light organ

time_t now; 
struct tm time_info;
SemaphoreHandle_t mutex;

/**
 * declare functions of the whole module
 * */
void timer_start_stop(void);
void timer_add_minutes(void);
void timer_reset(void);
void timer_add_seconds(void);
void stopwatch_start_stop(void);
void stopwatch_reset(void);
void alarm_start_stop(void);
void alarm_add_hours(void);
void alarm_reset(void);
void alarm_add_minutes(void);
void recording_play(void);
void recording_record(void);
void next_recording(void);
void previous_recording(void);
void increase_volume(void);
void decrease_volume(void);
void increase_amount(void);
void decrease_amount(void);
void print_volume_line();
void radio_start_stop(void);
void next_radio(void);
void previous_radio(void);
void say_time_task(void *pvParameter);
void say_timer_task(void *pvParameter);
void say_stopwatch_task(void *pvParameter);
void say_alarm_task(void *pvParameter);
void light_organ_task(void *pvParameters);
void light_organ_start_stop(void);

typedef struct
{
    char name[10];
    void (*body)(void);
} SUB_MENU_ITEM;

/**
 * array to connect function pointers for tasks in a submenu
 * */
SUB_MENU_ITEM sub_menu_function_list[10][4] = {
    {{"-", NULL}, {"-", NULL}, {"-", NULL}, {"-", NULL}}, //current time doens't have functions
    {{"Start/Stop", alarm_start_stop}, {"Hour+", alarm_add_hours}, {"Reset", alarm_reset}, {"Min+", alarm_add_minutes}}, //alarm has 4 functions
    {{"Start/Stop", timer_start_stop}, {"Min+", timer_add_minutes}, {"Reset", timer_reset}, {"Sec+", timer_add_seconds}}, //timer has 4 functions
    {{"Start/Stop", stopwatch_start_stop}, {"Reset", stopwatch_reset}, {"-", NULL}, {"-", NULL}}, //timer had 2 functions
    {{"Start/Stop", radio_start_stop}, {"Radio+", next_radio}, {"-", NULL}, {"Radio-", previous_radio}}, //radio has 3 functions
    {{"-", NULL}, {"-", NULL}, {"-", NULL}, {"-", NULL}}, //weather outside not implemented
    {{"-", NULL}, {"-", NULL}, {"-", NULL}, {"-", NULL}}, //weather inside not implemented
    {{"Play", recording_play}, {"Rec+", next_recording}, {"Record", recording_record}, {"Rec-", previous_recording}}, //recording has 4 functions
    {{"Vol+", increase_volume}, {"Amount+", increase_amount}, {"Vol-", decrease_volume}, {"Amount-", decrease_amount}}, //volume has 4 functions
    {{"Start/Stop", light_organ_start_stop}, {"-", NULL}, {"-", NULL}, {"-", NULL}}}; //light organ has 1 funciton

/**
 * function pointer to start/stop the timer.
 * */

void timer_start_stop(void)
{
    if (timer_enabled)
    {
        timer_enabled = 0;
        if (!timer_finished)
        {
            timer_set_minutes = minutes_remaining;
            timer_set_seconds = seconds_remaining;
        }
    }
    else
    {
        timer_enabled = 1;
        timer_finished = 0;
        minutes_remaining = timer_set_minutes;
        seconds_remaining = timer_set_seconds;
    }
}

/**
 * function pointer to reset the timer.
 * - when the timer is running: timer stops and is set to 0.
 * - when the timer is paused: the timer is set to 0
 * */
void timer_reset(void)
{
    if (timer_enabled)
    {
        timer_enabled = 0;
    }
    timer_set_minutes = 0;
    timer_set_seconds = 0;
    minutes_remaining = 0;
    seconds_remaining = 0;
    timer_finished = 1;
}

/**
 * function pointer to increase the amount of minutes of the timer.
 * you can't increase the amount of minutes when the timer is running
 * */
void timer_add_minutes(void)
{
    if (!timer_enabled)
    {
        timer_set_minutes++;
        if (timer_set_minutes > 59)
        {
            timer_set_minutes = 0;
        }
    }
}

/**
 * function pointer to increase the amount of seconds of the timer.
 * you can't increase the amount of seconds when the timer is running
 * */
void timer_add_seconds(void)
{
    if (!timer_enabled)
    {
        timer_set_seconds++;
        if (timer_set_seconds > 59)
        {
            timer_set_seconds = 0;
        }
    }
}

/**
 * function pointer to start/stop the stopwatch.
 * */
void stopwatch_start_stop(void)
{
    stopwatch_enabled ^= 1;
}

/**
 * function pointer to reset the stopwatch.
 * - when the stopwatch is running --> stopwatch stops and set to 0
 * - when the stopwatch is not running --> stopwatch is just set to 0
 * */
void stopwatch_reset(void)
{
    stopwatch_enabled = 0;
    stopwatch_minutes = 0;
    stopwatch_seconds = 0;
}

/**
 * function pointer to start/stop the alarm.
 * */
void alarm_start_stop(void)
{
    alarm_enabled ^= 1;
}

/**
 * function pointer to reset the alarm.
 * - when the alarm is enabled --> alarm will disable and set to 0
 * - when the alarm is disabled --> alarm is just set to 0
 * */
void alarm_reset(void)
{
    if (alarm_enabled)
    {
        alarm_enabled = 0;
    }
    alarm_hours = 0;
    alarm_minutes = 0;
}

/**
 * function pointer to increase the amount of hours of the alarm
 * hours can't be bigger than 23
 * */
void alarm_add_hours(void)
{
    if (!alarm_enabled)
    {
        alarm_hours++;
        if (alarm_hours > 23)
        {
            alarm_hours = 0;
        }
    }
}

/**
 * function pointer to increase the amount of minutes of the alarm
 * minutes can't be bigger than 59
 * */
void alarm_add_minutes(void)
{
    if (!alarm_enabled)
    {
        alarm_minutes++;
        if (alarm_minutes > 59)
        {
            alarm_minutes = 0;
        }
    }
}

/**
 * function pointer to increase the volume
 * when current volume + increase amount > 100, the volume will not change
 * */
void increase_volume(void)
{
    if (current_volume + increase_amount_volume <= 100)
    {
        current_volume += increase_amount_volume;
        set_volume(current_volume);
        print_volume_line(); //print the current volume and increase amount on the lcd
    }
}

/**
 * function pointer to decrease the volume
 * when current volume - decrease amount < 0, the volume will not change
 * */
void decrease_volume(void)
{
    if (current_volume - increase_amount_volume > 0)
    {
        current_volume -= increase_amount_volume;
        set_volume(current_volume);
        print_volume_line(); //print the current volume and increase amount on the lcd
    }
}

/**
 * function pointer to increase the increase/decrease amount of the volume
 * */
void increase_amount(void)
{
    increase_amount_volume++;
    if (increase_amount_volume > 99)
    {
        increase_amount_volume = 1;
    }
    print_volume_line(); //print the current volume and increase amount on the lcd
}

/**
 * function pointer to decrease the increase/decrease amount of the volume
 * */
void decrease_amount(void)
{
    increase_amount_volume--;
    if (increase_amount_volume < 1)
    {
        increase_amount_volume = 99;
    }
    print_volume_line(); //print the current volume and increase amount on the lcd
}

/**
 * function to print the current volume and increase amount on the second line of the lcd
 * */
void print_volume_line()
{
    clear_line(1);
    char volume_buffer[20];
    sprintf(volume_buffer, "Volume %d Amount %d", current_volume, increase_amount_volume);
    print_lcd(volume_buffer, 1, 0);
}

/**
 * function pointer to play the selected recording
 * */
void recording_play(void)
{
    play_recording(get_recording_filename(current_recording));
}

/**
 * function pointer to record the selected recording. The previous recording will be lost.
 * */
void recording_record(void)
{
    record_recording(get_recording_filename(current_recording));
}

/**
 * function pointer to select the next recording
 * */
void next_recording(void)
{
    clear_line(1);
    current_recording++;
    if (current_recording > 9)
    {
        current_recording = 0;
    }
    char buffer[20];
    size_t n = strlen(recording_files[current_recording]);
    sprintf(buffer, "%s", recording_files[current_recording]);
    print_lcd(buffer, 1, 9 - (n/2));
}

/**
 * function pointer to select the previous recording
 * */
void previous_recording(void)
{
    clear_line(1);
    current_recording--;
    if (current_recording < 0)
    {
        current_recording = 9;
    }
    char buffer[20];
    size_t n = strlen(recording_files[current_recording]);
    sprintf(buffer, "%s", recording_files[current_recording]);
    print_lcd(buffer, 1, 9 - (n/2));
}

/**
 * function pointer to start/stop the radio
 * */
void radio_start_stop(void)
{
    radio_is_running = !radio_is_running;
    if (radio_is_running)
    {
        HK_radio_init(current_radio_index);
    }
    else
    {
        HK_radio_deinit();
    }
}

/**
 * function pointer to select the next radiostation
 * */
void next_radio(void)
{
    clear_line(1);
    current_radio_index++;
    if (current_radio_index > RADIO_ITEMS - 1)
    {
        current_radio_index = 0;
    }
    char buffer[20];
    size_t n = strlen(radio_list[current_radio_index].name);
    sprintf(buffer, radio_list[current_radio_index].name);
    print_lcd(buffer, 1, 9 - (n/2));

    if (radio_is_running)
    {
        HK_radio_set(current_radio_index);
    }
}

/**
 * function pointer to select the previous radiostation
 * */
void previous_radio(void)
{
    clear_line(1);
    current_radio_index--;
    if (current_radio_index < 0)
    {
        current_radio_index = RADIO_ITEMS - 1;
    }

    char buffer[20];
    size_t n = strlen(radio_list[current_radio_index].name);
    sprintf(buffer, radio_list[current_radio_index].name);
    print_lcd(buffer, 1, 9 - (n/2));

    if (radio_is_running)
    {
        HK_radio_set(current_radio_index);
    }
}

/**
 * function pointer to start/stop the light organ
 * - lightorgan can't be started when radio is on.
 * */
void light_organ_start_stop(void)
{
    clear_line(1);
    if (light_organ_enabled)
    {
        light_organ_enabled = !light_organ_enabled;
        set_running_enabled(light_organ_enabled);
        char buffer[20];
        size_t n = strlen("Off");
        sprintf(buffer, "Off");
        print_lcd(buffer, 1, 9 - (n/2));
    }
    else
    {
        if (!radio_is_running)
        {
            light_organ_enabled = !light_organ_enabled;
            char buffer[20];
            size_t n = strlen("On");
            sprintf(buffer, "On");
            print_lcd(buffer, 1, 9 - (n/2));
            set_running_enabled(light_organ_enabled);
            init_light_organ();
            xTaskCreate(light_organ_task, "light_organ_task", 1024 * 10, NULL, 10, NULL);
        }
    }
}

/**
 * function that returns the index of the array when an button is pressed.
 * */
int button_to_index(int states)
{
    int index = 0;
    switch (states)
    {
    case BUTTON_MAIN_LEFT:
        index = FUNCTIONALITY_BUTTON_1;
        break;
    case BUTTON_MAIN_RIGHT:
        index = FUNCTIONALITY_BUTTON_2;
        break;
    case BUTTON_SUB_LEFT:
        index = FUNCTIONALITY_BUTTON_3;
        break;
    case BUTTON_SUB_RIGHT:
        index = FUNCTIONALITY_BUTTON_4;
        break;
    default:
        break;
    }
    return index;
}

/**
 * function that returns the text of an task inside the submenu
 * */
char *get_function_text(int index, int functionIndex)
{
    return sub_menu_function_list[index][functionIndex].name;
}

/**
 * function that returns the filenames of an recording
 * */
char *get_recording_filename(int index)
{
    return recording_files[index];
}

/**
 * function that returns the index of the selected recording
 * */
int get_current_recording_index()
{
    return current_recording;
}

/**
 * function that returns the name of the selected radiostation
 * */
char *get_current_radio()
{
    return radio_list[current_radio_index].name;
}

/**
 * function that returns the state of the light organ
 * */
char *get_light_organ_state()
{
    if (light_organ_enabled)
    {
        return "On";
    }
    else
    {
        return "Off";
    }
}

/**
 * task to handle the pressed buttons
 * */
void mcp23017_task_read(void *pvParameters)
{
    while (1)
    {
        vTaskDelay(100 / portTICK_RATE_MS);
        uint8_t states = 0x00;
        xSemaphoreTake(mutex, portMAX_DELAY); //take the mutex
        mcp23017_err_t ret = mcp23017_read_register(&mcp23017, MCP23017_GPIO, GPIOB, &states); //read the states of the buttons
        if (ret == MCP23017_ERR_OK)
        {
            //ESP_LOGI(TAG, "GPIO register B states: %d", states);
        }
        else
        {
            ESP_LOGI(TAG, "GPIO register B error: %d", ret);
        }

        if (last_button_state != states) //check to prevent that a fast navigation through the menu
        {
            //handleMainMenu(states);

            if (get_in_submenu_bool() == 0) //in the main menu
            {
                handle_menu(states);
            }
            else //when in the submenu of a functionality
            {
                if (states == BACK_BUTTON)
                {
                    handle_menu(states);
                }
                else
                {
                    if (states != 253 && sub_menu_function_list[get_current_menu_index()][button_to_index(states)].body != NULL)
                    {
                        (*sub_menu_function_list[get_current_menu_index()][button_to_index(states)].body)(); //call the function pointer  of the pressed button
                    }
                }
            }
            last_button_state = states;
        }
        xSemaphoreGive(mutex); //free the mutex
    }
    vTaskDelete(NULL); //delete the task
}

/**
 * task to handle the RGB-led
 * */
void mcp23017_task_write(void *pvParameters)
{
    while (1)
    {
        xSemaphoreTake(mutex, portMAX_DELAY); //take the mutex
        mcp23017_write_register(&mcp23017, MCP23017_GPIO, GPIOA, get_current_color()); //write the current color to the RGB
        xSemaphoreGive(mutex); //free the mutex
        //on = !on;
        vTaskDelay(50 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL); //delete the task
}

/**
 * function to initialize the mcp23017 module
 * */
void init_mcp23017(void)
{
    // Initialize I2C bus
    mcp23017.i2c_addr = 0x20;
    mcp23017.sda_pin = 18;
    mcp23017.scl_pin = 23;
    mcp23017.sda_pullup_en = GPIO_PULLUP_ENABLE;
    mcp23017.scl_pullup_en = GPIO_PULLUP_ENABLE;
    //ESP_ERROR_CHECK(mcp23017_init(&mcp23017));

    // Set GPIO Direction
    mcp23017_write_register(&mcp23017, MCP23017_IODIR, GPIOA, 0x00); // full port on OUTPUT
    mcp23017_write_register(&mcp23017, MCP23017_IODIR, GPIOB, 0xFD); // full port on INPUT
    mcp23017_write_register(&mcp23017, MCP23017_GPPU, GPIOB, 0xFD);  // Enable pullup on B0 - 7
}

/**
 * function to handle the current time
 * */
void handle_current_time()
{
    time(&now);
    char strftime_buf[20];
    localtime_r(&now, &time_info);
    sprintf(&strftime_buf[0], "%02d:%02d:%02d", time_info.tm_hour, time_info.tm_min, time_info.tm_sec);

    if (get_in_submenu_bool() == 1 && get_current_menu_index() == SUBMENU_CLOCK_CURRENT_INDEX) //when in the submenu of the current time
    {
        size_t n = strlen("..:..:..");
        print_lcd(strftime_buf, 1, 9 - (n/2)); //print the current time on the second line of the lcd

        if (time_info.tm_sec % 60 == 0)
        {
            xTaskCreate(say_time_task, "say_time_task", 1024 * 2, NULL, 10, NULL); //pronounce the time every minute
        }
    }
}

/**
 * function to handle the timer
 * 
 * NOTE -- Logic of the timer is implemented, audio is not implemented.
 * 
 * minutes: 0 - 59, seconds 0 - 59
 * */
void handle_timer()
{
    char timer_buffer[6];
    if (timer_enabled)
    {
        if (!timer_finished)
        {
            seconds_remaining--;
            if (seconds_remaining < 0)
            {
                minutes_remaining--;
                seconds_remaining += 60;
            }
            if (minutes_remaining <= 0 && seconds_remaining <= 0) //timer is finished
            { 
                //speel een geluidje af of spreek uit: "timer is afgelopen!"
                timer_finished = 1;
                timer_enabled = 0;
                ESP_LOGI(TAG, "Timer finished");
            }
            else if (minutes_remaining == 1 && seconds_remaining == 0) //one minute left
            { 
                //spreek uit: "nog 1 minuut"
                ESP_LOGI(TAG, "Timer one minute left");
            }
            else if (minutes_remaining == 0 && seconds_remaining == 10) //ten seconds left
            { 
                //spreek uit: "nog 10 seconden"
                ESP_LOGI(TAG, "Timer ten seconds left");
            }
        }
        sprintf(timer_buffer, "%02d:%02d", minutes_remaining, seconds_remaining);
    }
    else
    {
        sprintf(timer_buffer, "%02d:%02d", timer_set_minutes, timer_set_seconds);
    }
    if (get_in_submenu_bool() == 1 && get_current_menu_index() == SUBMENU_CLOCK_TIMER_INDEX) //when in the submenu of the timer
    {
        size_t n = strlen("..:..");
        print_lcd(timer_buffer, 1, 9 - (n/2)); //print the current timer on the second line of the lcd
    }
}

/**
 * function to handle the timer
 * 
 * NOTE -- Logic of the stopwatch is implemented, audio is not implemented.
 * 
 * minutes: 0 - 59, seconds: 0 - 59
 * */
void handle_stopwatch()
{
    char stopwatch_buffer[6];
    if (stopwatch_enabled)
    {
        stopwatch_seconds++;
        if (stopwatch_seconds >= 60)
        {
            stopwatch_minutes++;
            stopwatch_seconds -= 60;
        }
        if (stopwatch_seconds == 0 && stopwatch_minutes != 0) //pronounce the stopwatch every minute
        { 
            //spreek uit: "... minuut"
            ESP_LOGI(TAG, "Timer reached %d minute", stopwatch_minutes);
        }
    }
    sprintf(stopwatch_buffer, "%02d:%02d", stopwatch_minutes, stopwatch_seconds);
    if (get_in_submenu_bool() == 1 && get_current_menu_index() == SUBMENU_CLOCK_STOPWATCH_INDEX) //when in the submenu of the stopwatch
    {
        size_t n = strlen("..:..");
        print_lcd(stopwatch_buffer, 1, 9 - (n/2)); //print the current timer on the second line of the lcd
    }
}

/**
 * function to handle the alarm
 * 
 * NOTE -- Logic of the alarm is implemented, audio is not implemented.
 * 
 * hours: 0 - 23, minutes: 0 - 59
 * */
void handle_alarm()
{
    char alarm_buffer[6];
    if (alarm_enabled)
    {
        time_t now;
        struct tm time_info;
        time(&now); //get the current time
        localtime_r(&now, &time_info); //synchronize the current time

        if (time_info.tm_hour == alarm_hours && time_info.tm_min == alarm_minutes) // when the alarm is reached
        {
            //Maak een geluid
            ESP_LOGI("handle_alarm", "alarm finished!");
        }
    }
    sprintf(alarm_buffer, "%02d:%02d", alarm_hours, alarm_minutes);
    if (get_in_submenu_bool() == 1 && get_current_menu_index() == SUBMENU_CLOCK_ALARM_INDEX) //when in the submenu of the alarm
    {
        print_lcd(alarm_buffer, 1, 3); //print the current timer on the second line of the lcd
        if (alarm_enabled)
        {
            int n = strlen("Enabled");
            print_lcd("Enabled", 1, 20 - n); //print the state of the alarm --> enabled
        }
        else
        {
            int n = strlen("Disabled");
            print_lcd("Disabled", 1, 20 - n); //print the state of the alarm --> disabled
        }
    }
}

/**
 * 1 timer callback to handle the current time, timer, stopwatch and alarm
 * */
void timer_1_sec_callback(TimerHandle_t xTimer)
{
    handle_current_time();
    handle_timer();
    handle_stopwatch();
    handle_alarm();
}

/**
 * task to pronounce the current time
 * */
void say_time_task(void *pvParameters)
{
    if (radio_is_running) //when radio is running
    {
        HK_radio_deinit(); //stop radio
        vTaskDelay(500 / portTICK_RATE_MS);
        AOC_say_time(now, time_info); //pronounce time
        vTaskDelay(7000 / portTICK_RATE_MS);
        HK_radio_init(current_radio_index); // start radio
    }
    else
    {
        AOC_say_time(now, time_info);
    }
    vTaskDelete(NULL); //delete the task
}

/**
 * task to handle the light organ
 * */
void light_organ_task(void *pvParameters)
{
    tone_detection_task(); //start the tonedetection function
    vTaskDelete(NULL);
}

/**
 * function to initialize the wifi
 * */
void init_flash(void)
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 1, 0))
    ESP_ERROR_CHECK(esp_netif_init());
#else
    tcpip_adapter_init();
#endif
}

void app_main(void)
{
    ESP_LOGI(TAG, "init_flash");
    init_flash(); //initialize the wifi

    ESP_LOGI(TAG, "clock_init");
    clock_init(); //initialize the clock module

    ESP_LOGI(TAG, "SD_init");
    init_SD(); //initialize the sdcard

    ESP_LOGI(TAG, "AOC_init");
    AOC_init(); //initialize the talking clock

    ESP_LOGI(TAG, "init_volume");
    init_volume(); //initialize the volume 
    current_volume = get_volume();

    ESP_LOGI(TAG, "i2c_master_init");
    i2c_master_init(); //initialize the lcd

    ESP_LOGI(TAG, "Print menu for the first time!");
    print_menu(); //print the first menu
    

    // Initialize 1 second timer to display the time
    int id = 1;
    timer_1_sec = xTimerCreate("MyTimer", pdMS_TO_TICKS(1000), pdTRUE, (void *)id, &timer_1_sec_callback);
    if (xTimerStart(timer_1_sec, 10) != pdPASS)
    {
        ESP_LOGE(TAG, "Cannot start 1 second timer");
    }

    mutex = xSemaphoreCreateMutex(); //generate a mutex
    init_mcp23017(); //initialize the GPIO 

    set_current_color(0x0E); //set the led white
    xTaskCreate(mcp23017_task_write, "mcp23017_task_write", 1024 * 2, NULL, 10, NULL); //start the write task to handle the RGB-led
    xTaskCreate(mcp23017_task_read, "mcp23017_task_read", 1024 * 10, NULL, 2, NULL); //start the read task to handle the buttons

    while (1)
    {
        vTaskDelay(500 / portTICK_RATE_MS);
    }
}
