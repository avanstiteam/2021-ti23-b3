#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_http_client.h"
#include "sdkconfig.h"
#include "string.h"

#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "sntp_sync.h"
#include <sys/time.h>

#if __has_include("esp_idf_version.h")
#include "esp_idf_version.h"
#else
#define ESP_IDF_VERSION_VAL(major, minor, patch) 1
#endif

#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 1, 0))
#include "esp_netif.h"
#else
#include "tcpip_adapter.h"
#endif

void stmp_timesync_event(struct timeval *tv)
{
    ESP_LOGI("CLOCK", "Notification of a time synchronization event");
	
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI("CLOCK", "The current date/time in Amsterdam is: %s", strftime_buf);
	
}

//get time
void clockInit(void){
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    esp_periph_set_handle_t set = esp_periph_set_init(&periph_cfg);
    periph_wifi_cfg_t wifi_cfg = {
        .ssid = CONFIG_WIFI_SSID,
        .password = CONFIG_WIFI_PASSWORD,
    };

    ESP_LOGI("CLOCK_INIT", "get wifi handle");
    esp_periph_handle_t wifi_handle = periph_wifi_init(&wifi_cfg);
    ESP_LOGI("CLOCK_INIT", "Starting peripheral");
    esp_periph_start(set, wifi_handle);
    ESP_LOGI("CLOCK_INIT", "wifi wait for connected");
    periph_wifi_wait_for_connected(wifi_handle, portMAX_DELAY);

       // Synchronize NTP time
    ESP_LOGI("CLOCK_INIT", "setting callback");
	sntp_sync(stmp_timesync_event);
    
    
}

// Init ESP-32 Flash configuration
// Needed for WiFi
void init_flash(void) {
	esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
}



void app_main(void)
{
    ESP_LOGI("CLOCK", "InitFlash");
    init_flash();

    ESP_LOGI("CLOCK", "TCPIP init");
   #if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 1, 0))
    ESP_ERROR_CHECK(esp_netif_init());
    #else
    tcpip_adapter_init();
    #endif

    ESP_LOGI("CLOCK", "Init Clock");
    clockInit();

    ESP_LOGI("CLOCK", "Setup Complete");
    while (1)
    {    
        time_t now;
        struct tm timeinfo;
        time(&now);
        
        char strftime_buf[20];
        localtime_r(&now, &timeinfo);
        sprintf(&strftime_buf[0], "%02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
        printf(strftime_buf);
        ESP_LOGI("CLOCK", "%02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);

        vTaskDelay(1000/ portTICK_PERIOD_MS);
    }
}



