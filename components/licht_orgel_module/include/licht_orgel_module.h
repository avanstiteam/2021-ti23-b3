#ifndef LICHT_ORGEL_MODULE_H
#define LICHT_ORGEL_MODULE_H

esp_err_t tone_detection_task();

//This function returns the current color to the main
int get_current_color();

//This function sets the current color
void set_current_color();

//This function initializes the light organ
void init_light_organ();

//This function sets the state of the light organ
void set_running_enabled(int state);

#endif