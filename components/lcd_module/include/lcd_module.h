#ifndef LCD_MODULE_H
#define LCD_MODULE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "sdkconfig.h"

#define FUNCTIONALITY_BUTTON_1 0
#define FUNCTIONALITY_BUTTON_2 1
#define FUNCTIONALITY_BUTTON_3 2
#define FUNCTIONALITY_BUTTON_4 3

#define BUTTON_MAIN_LEFT 252
#define BUTTON_MAIN_RIGHT 249
#define BUTTON_SUB_LEFT 245
#define BUTTON_SUB_RIGHT 237
#define OK_BUTTON 221
#define BACK_BUTTON 189

#define SUBMENU_CLOCK_CURRENT_INDEX 0
#define SUBMENU_CLOCK_ALARM_INDEX 1
#define SUBMENU_CLOCK_TIMER_INDEX 2
#define SUBMENU_CLOCK_STOPWATCH_INDEX 3
#define SUBMENU_RADIO_INDEX 4
#define SUBMENU_WEATHER_INSIDE_INDEX 5
#define SUBMENU_WEATHER_OUTSIDE_INDEX 6
#define SUBMENU_RECORDINGS_INDEX 7
#define SUBMENU_VOLUME_INDEX 8
#define SUBMENU_LIGHT_ORGAN_INDEX 9

void i2c_master_init();
void handle_menu(int button);

//functions for printing the lcd
void print_menu();
void print_lcd(char *text, int line, int column);
void clear_lcd();
void clear_line(int line);
void print_volume_line();

//getters
int get_in_submenu_bool();
int get_current_menu_index();
char* get_function_text(int index, int function);
char* get_recording_filename(int index);
int get_current_recording_index();
char* get_current_radio();
char* get_light_organ_state();

#endif