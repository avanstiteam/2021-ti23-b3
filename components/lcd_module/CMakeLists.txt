set(COMPONENT_ADD_INCLUDEDIRS include)
set(COMPONENT_SRCS "lcd_module.c")

set(COMPONENT_PRIV_REQUIRES "esp_peripherals" "esp32-i2c-lcd1602" "esp32-smbus")

register_component()
