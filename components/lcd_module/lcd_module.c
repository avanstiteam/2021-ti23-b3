/**
 * Standard c includes needed for this module
 * */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/**
 * include selfmade header files
 * */
#include "lcd_module.h"

/**
 * include freeRTOS libraries
 * */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "i2c-lcd1602.h"
#include "smbus.h"
#include "esp32/rom/uart.h"

#define LCD_MAX_LINES 4;
#define LCD_MAX_CHARACTERS 20
#define MAINMENU_FUNCTIONAMOUNT 3

#define SDA_GPIO 18
#define SCL_GPIO 23

#define I2C_ADDR_LCD 0x27

#define MAINMENU_BUTTON_LEFT 0
#define MAINMENU_BUTTON_RIGHT 1
#define SUBMENU_BUTTON_LEFT 2
#define SUBMENU_BUTTON_RIGHT 3
#define BUTTON_OK 3
#define BUTTON_BACK 4

#define SUBMENU_CLOCK_CURRENT_ID 1
#define SUBMENU_CLOCK_TIMER_ID 2
#define SUBMENU_CLOCK_STOPWATCH_ID 3
#define SUBMENU_CLOCK_ALARM_ID 4
#define SUBMENU_RADIO_CHANGERADIO_ID 5
#define SUBMENU_WEATHER_INSIDE_ID 6
#define SUBMENU_WEATHER_OUTSIDE_ID 7
#define SUBMENU_RECORDING_RECORD_ID 8
#define SUBMENU_VOLUME_ID 9
#define SUBMENU_LIGHT_ORGAN_ID 10

#define BUTTON_MAIN_LEFT 252
#define BUTTON_MAIN_RIGHT 249
#define BUTTON_SUB_LEFT 245
#define BUTTON_SUB_RIGHT 237
#define OK_BUTTON 221
#define BACK_BUTTON 189

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#define I2C_MASTER_FREQ_HZ       100000
#define I2C_MASTER_SDA_IO        18
#define I2C_MASTER_SCL_IO        23
#define LCD_NUM_ROWS			 4
#define LCD_NUM_COLUMNS			 40
#define LCD_NUM_VIS_COLUMNS		 20

#define TAG "LCD_MODULE"

static int current_menu_index = 0; //variable to store the index of the selected menu
static int current_menu_id; //variable to store the id of the selected menu
static int in_submenu = 0; //variable to store the state of the menu: in mainmenu or in submenu

i2c_lcd1602_info_t * lcd_info;

/**
 * declare functions of the whole module
 * */
void clock_current(void);
void clock_timer(void);
void clock_stopwatch(void);
void clock_alarm(void);
void radio(void);
void weather_inside(void);
void weather_outside(void);
void recording_record(void);
void recording_record_play(void);
void change_volume(void);
void light_organ(void);

typedef struct  { //struct which contains information of a submenu
    unsigned int id;
    unsigned int newId[4];
    char* text[2];
    void (*onEntry)(void);
} MENU_ITEM_STRUCT;

MENU_ITEM_STRUCT menu[] = {
    {
        SUBMENU_CLOCK_CURRENT_ID, //current time is current submenu
        {
            SUBMENU_WEATHER_OUTSIDE_ID, //top left pressed: go to weather outside submenu
            SUBMENU_LIGHT_ORGAN_ID, //top right pressed: go to light organ submenu
            SUBMENU_CLOCK_STOPWATCH_ID, // bottom left pressed: go to stopwatch submenu
            SUBMENU_CLOCK_ALARM_ID //bottom right pressed: go to alarm submenu
        },
        {
            "Clock",
            "Current time"
        },
        clock_current
    },
    {
        SUBMENU_CLOCK_ALARM_ID, //alarm is current submenu
        {
            SUBMENU_WEATHER_OUTSIDE_ID, //top left pressed: go to weather outside submenu
            SUBMENU_LIGHT_ORGAN_ID, //top right pressed: go to light organ submenu
            SUBMENU_CLOCK_CURRENT_ID, // bottom left pressed: go to current time submenu
            SUBMENU_CLOCK_TIMER_ID //bottom right pressed: go to timer submenu
        },
        {
            "Clock",
            "Alarm"
        },
        clock_alarm
    },
    {
        SUBMENU_CLOCK_TIMER_ID, //timer is current submenu
        {
            SUBMENU_WEATHER_OUTSIDE_ID, //top left pressed: go to weather outside submenu
            SUBMENU_LIGHT_ORGAN_ID, //top right pressed: go to light organ submenu
            SUBMENU_CLOCK_ALARM_ID, // bottom left pressed: go to alarm submenu
            SUBMENU_CLOCK_STOPWATCH_ID //bottom right pressed: go to stopwatch submenu
        },
        {
            "Clock",
            "Timer"
        },
        clock_timer
    },
    {
        SUBMENU_CLOCK_STOPWATCH_ID, //stopwatch is current submenu
        {
            SUBMENU_WEATHER_OUTSIDE_ID, //top left pressed: go to weather outside submenu
            SUBMENU_LIGHT_ORGAN_ID, //top right pressed: go to light organ submenu
            SUBMENU_CLOCK_TIMER_ID, // bottom left pressed: go to timer submenu
            SUBMENU_CLOCK_CURRENT_ID //bottom right pressed: go to current time submenu
        },
        {
            "Clock",
            "Stopwatch"
        },
        clock_stopwatch
    },
    {
        SUBMENU_RADIO_CHANGERADIO_ID, //radio is current submenu
        {
            SUBMENU_LIGHT_ORGAN_ID, //top left pressed: go to light organ submenu
            SUBMENU_RECORDING_RECORD_ID, //top right pressed: go to recordings submenu
            SUBMENU_RADIO_CHANGERADIO_ID, // bottom left pressed: go to radio submenu
            SUBMENU_RADIO_CHANGERADIO_ID //bottom right pressed: go to radio submenu
        },
        {
            "Radio",
            "Change radio"
        },
        radio
    },
    {
        SUBMENU_WEATHER_OUTSIDE_ID, //weather outside is current submenu
        {
            SUBMENU_VOLUME_ID, //top left pressed: go to volume submenu
            SUBMENU_CLOCK_CURRENT_ID, //top right pressed: go to current time submenu
            SUBMENU_WEATHER_INSIDE_ID, // bottom left pressed: go to weather inside submenu
            SUBMENU_WEATHER_INSIDE_ID //bottom right pressed: go to weather inside submenu
        },
        {
            "Weather",
            "Outside"
        },
        weather_outside
    },
    {
        SUBMENU_WEATHER_INSIDE_ID, //weather inside is current submenu
        {
            SUBMENU_VOLUME_ID, //top left pressed: go to volume submenu
            SUBMENU_CLOCK_CURRENT_ID, //top right pressed: go current time submenu
            SUBMENU_WEATHER_OUTSIDE_ID, // bottom left pressed: go to weather outside submenu
            SUBMENU_WEATHER_OUTSIDE_ID //bottom right pressed: go to weather outside submenu
        },
        {
            "Weather",
            "Inside"
        },
        weather_inside
    },
    {
        SUBMENU_RECORDING_RECORD_ID, //recordings is current submenu
        {
            SUBMENU_RADIO_CHANGERADIO_ID, //top left pressed: go to weather outside
            SUBMENU_VOLUME_ID, //top right pressed: go to volume submenu
            SUBMENU_RECORDING_RECORD_ID, // bottom left pressed: go to recordings submenu
            SUBMENU_RECORDING_RECORD_ID //bottom right pressed: go to recordings submenu
        },
        {
            "Recordings",
            "Record/play"
        },
        recording_record_play
    },
    {
        SUBMENU_VOLUME_ID, //volume is current submenu
        {
            SUBMENU_RECORDING_RECORD_ID, //top left pressed: go to weather outside
            SUBMENU_WEATHER_OUTSIDE_ID, //top right pressed: go to weather outside submenu
            SUBMENU_VOLUME_ID, // bottom left pressed: go to volume submenu
            SUBMENU_VOLUME_ID //bottom right pressed: go to volume submenu
        },
        {
            "Volume",
            "Change volume"
        },
        change_volume
    },
    {
        SUBMENU_LIGHT_ORGAN_ID, //light organ is current submenu
        {
            SUBMENU_CLOCK_CURRENT_ID, //top left pressed: go to weather outside
            SUBMENU_RADIO_CHANGERADIO_ID, //top right pressed: go to radio submenu
            SUBMENU_LIGHT_ORGAN_ID, // bottom left pressed: go to light organ submenu
            SUBMENU_LIGHT_ORGAN_ID //bottom right pressed: go to light organ submenu
        },
        {
            "Light organ",
            "Turn on"
        },
        light_organ
    }
};

/**
 * function called when going in submenu
 * */
void enter_function(){ 
    in_submenu = 1;
    (*menu[current_menu_index].onEntry)();
}

/**
 * function called when leaving submenu
 * */
void exit_function(){
    in_submenu = 0;
    print_menu();
}

/**
 * this function returns the bool in_submenu to the main
 * */
int get_in_submenu_bool(){
    return in_submenu;
}

/**
 * this function returns the current_menu_index to the main
 * */
int get_current_menu_index(){
    return current_menu_index;
}

/**
 * this function handles the pressed buttons
 * */
void handle_menu(int button){
    ESP_LOGI(TAG, "Buttons state: %d", button);
    int buttonPressed = 0;
    switch (button){
        case BUTTON_MAIN_LEFT: //button top left pressed
            current_menu_id = menu[current_menu_index].newId[MAINMENU_BUTTON_LEFT];
            buttonPressed = 1;
            break;

        case BUTTON_MAIN_RIGHT: //button top right pressed
            current_menu_id = menu[current_menu_index].newId[MAINMENU_BUTTON_RIGHT];
            buttonPressed = 1;
            break;

        case BUTTON_SUB_LEFT: //button bottom left pressed
            current_menu_id = menu[current_menu_index].newId[SUBMENU_BUTTON_LEFT];
            buttonPressed = 1;
            break;

        case BUTTON_SUB_RIGHT: //button bottom right pressed
            current_menu_id = menu[current_menu_index].newId[SUBMENU_BUTTON_RIGHT];
            buttonPressed = 1;
            break;
    
        case OK_BUTTON: //ok button pressed
            enter_function();
            break;

        case BACK_BUTTON: //back button pressed
            exit_function();
            break;

        default:
            //No button pressed
             buttonPressed = 0;
            break;
        }


    if (buttonPressed){
        current_menu_index = 0;
        while (menu[current_menu_index].id != current_menu_id) {
            current_menu_index += 1;
        }
        ESP_LOGI(TAG, "CurrentMainMenyIndex: %d", current_menu_index);
        print_menu(); //print the new selected menu
    }
    
}

/**
 * this function prints a given string on a given linenumber
 * */
void print_lcd(char *text, int line, int column){
    ESP_LOGI(TAG, "print_lcd()");
    i2c_lcd1602_move_cursor(lcd_info, column, line);
    i2c_lcd1602_write_string(lcd_info, text);
}

/**
 * this function cleares the whole lcd
 * */
void clear_lcd(){
    ESP_LOGI(TAG, "Clear_lcd()");
    i2c_lcd1602_clear(lcd_info);
}

/**
 * this function cleares a given line
 * */
void clear_line(int line){
    print_lcd("                    ", line, 0);
}

/**
 * this function prints the whole main menu
 * */
void print_menu(){
    clear_lcd(); //clear lcd
    char buffer[20];
    size_t n = strlen(menu[current_menu_index].text[0]); //calculate length of string to write in the middle of the line
    sprintf(buffer, "< %s >", menu[current_menu_index].text[0]);
    
    print_lcd(buffer, 1, 9 - (n/2) - 1);
    
    print_lcd("--------------------", 2, 0);

    n = strlen(menu[current_menu_index].text[1]); //calculate length of string to write in the middle of the lcd
    sprintf(buffer, "< %s >", menu[current_menu_index].text[1]);
    print_lcd(buffer, 3, 9 - (n/2) - 1);
}

/**
 * this function pointer prints the whole submenu of the current time.
 * */
void clock_current(void){
    clear_lcd(); //clear the lcd
    char* title = "CURRENT TIME"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title
    
    print_lcd(get_function_text(SUBMENU_CLOCK_CURRENT_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_CLOCK_CURRENT_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_CLOCK_CURRENT_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_CLOCK_CURRENT_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_CLOCK_CURRENT_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_CLOCK_CURRENT_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the timer.
 * */
void clock_timer(void){
    clear_lcd(); //clear the lcd
    char* title = "TIMER"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title
    
    print_lcd(get_function_text(SUBMENU_CLOCK_TIMER_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_CLOCK_TIMER_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_CLOCK_TIMER_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_CLOCK_TIMER_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_CLOCK_TIMER_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_CLOCK_TIMER_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the stopwatch.
 * */
void clock_stopwatch(void){
    clear_lcd(); //clear the lcd
    char* title = "STOPWATCH"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title
    
    print_lcd(get_function_text(SUBMENU_CLOCK_STOPWATCH_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_CLOCK_STOPWATCH_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_CLOCK_STOPWATCH_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_CLOCK_STOPWATCH_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_CLOCK_STOPWATCH_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_CLOCK_STOPWATCH_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the alarm.
 * */
void clock_alarm(void){
    clear_lcd(); //clear the lcd
    char* title = "ALARM"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title
    
    print_lcd(get_function_text(SUBMENU_CLOCK_ALARM_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_CLOCK_ALARM_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_CLOCK_ALARM_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_CLOCK_ALARM_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_CLOCK_ALARM_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_CLOCK_ALARM_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the weather inside.
 * */
void weather_inside(void){
    clear_lcd(); //clear the lcd
    char* title = "WEATHER INSIDE"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title
    
    print_lcd(get_function_text(SUBMENU_WEATHER_INSIDE_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_WEATHER_INSIDE_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_WEATHER_INSIDE_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_WEATHER_INSIDE_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_WEATHER_INSIDE_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_WEATHER_INSIDE_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the weather outside.
 * */
void weather_outside(void){
    clear_lcd(); //clear the lcd
    char* title = "WEATHER OUTSIDE"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title
    
    print_lcd(get_function_text(SUBMENU_WEATHER_OUTSIDE_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_WEATHER_OUTSIDE_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_WEATHER_OUTSIDE_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_WEATHER_OUTSIDE_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_WEATHER_OUTSIDE_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_WEATHER_OUTSIDE_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the radio.
 * */
void radio(void){
    clear_lcd(); //clear the lcd
    char* title = "RADIO"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title

    print_lcd(get_current_radio(), 1, 0); //print the current selected radio

    print_lcd(get_function_text(SUBMENU_RADIO_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_RADIO_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_RADIO_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_RADIO_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_RADIO_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_RADIO_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the recordings.
 * */
void recording_record_play(void){
    clear_lcd(); //clear the lcd
    char* title = "Recordings"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title

    print_lcd(get_recording_filename(get_current_recording_index()), 1, 0); //print the current selected recording

    print_lcd(get_function_text(SUBMENU_RECORDINGS_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_RECORDINGS_INDEX, 1)); 
    print_lcd(get_function_text(SUBMENU_RECORDINGS_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_RECORDINGS_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_RECORDINGS_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_RECORDINGS_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the volume.
 * */
void change_volume(void){
    clear_lcd(); //clear the lcd
    char* title = "Change volume"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title

    print_volume_line(); //print the current volume and increase amount

    print_lcd(get_function_text(SUBMENU_VOLUME_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_VOLUME_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_VOLUME_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_VOLUME_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_VOLUME_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_VOLUME_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function pointer prints the whole submenu of the light organ.
 * */
void light_organ(){
    clear_lcd(); //clear the lcd
    char* title = "light organ"; //title of the submenu
    size_t n = strlen(title); //calculate the length of the title
    print_lcd(title, 0, 9 - (n/2)); //print the title

    print_lcd(get_light_organ_state(), 1, 0); //print the current state of the light organ

    print_lcd(get_function_text(SUBMENU_LIGHT_ORGAN_INDEX, 0), 2, 0); //print functionality 1
    n = strlen(get_function_text(SUBMENU_LIGHT_ORGAN_INDEX, 1));
    print_lcd(get_function_text(SUBMENU_LIGHT_ORGAN_INDEX, 1), 2, 20-n); //print functionality 2
    print_lcd(get_function_text(SUBMENU_LIGHT_ORGAN_INDEX, 2), 3, 0); //print functionality 3
    n = strlen(get_function_text(SUBMENU_LIGHT_ORGAN_INDEX, 3));
    print_lcd(get_function_text(SUBMENU_LIGHT_ORGAN_INDEX, 3), 3, 20-n); //print functionality 4
}

/**
 * this function initializes the lcd
 * */
void i2c_master_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);

    i2c_port_t i2c_num = I2C_MASTER_NUM;
    uint8_t address = 0x27;

    // Set up the SMBus
    smbus_info_t * smbus_info = smbus_malloc();
    smbus_init(smbus_info, i2c_num, address);
    smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS);

    // Set up the LCD1602 device with backlight off
    lcd_info = i2c_lcd1602_malloc();
    i2c_lcd1602_init(lcd_info, smbus_info, true, LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VIS_COLUMNS);

    // turn on backlight
    ESP_LOGI(TAG, "backlight on");
    //_wait_for_user();
    i2c_lcd1602_set_backlight(lcd_info, true);
}