#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"

#include "esp_peripherals.h"
#include "periph_sdcard.h"

audio_pipeline_handle_t pipeline;
audio_element_handle_t i2s_stream_writer, mp3_decoder;
audio_event_iface_handle_t evt;
esp_periph_set_handle_t set;

/**

	initAO();
	Initializes the Audio Output.

**/
void initAO(){
	/* Setting up config for periph */
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG(); // Using default esp periph config
	set = esp_periph_set_init(&periph_cfg);
	
	/* Initializing sdcard */
	periph_sdcard_init(set, SD_MODE_1_LINE); // Init sdcard on 1 line
	
	/* Audio board handle set up */
    audio_board_handle_t board_handle = audio_board_init();
    audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);
	/* Init audio codec to decode */

	/* Pipeline config init and assertion */
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG(); // Using default audio pipeline config
    pipeline = audio_pipeline_init(&pipeline_cfg);
    men_assert(pipeline);
	
	/* fatfs stream config and initialization */
	fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT(); // Using default fatfs stream config
	fatfs_cfg.type = AUDIO_STREAM_READER;
	fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);

	/* i2s stream config and initialization */
    i2s_stream_cfg_t i2_cfg = I2S_STREAM_CFG_DEFAULT(); // Using default i2s stream config
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2_cfg);

	/* mp3 decoder config and initialization */
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG(); // Using default mp3 decoder config
    mp3_decoder = mp3_decoder_init(&mp3_cfg);


	/* 
	Registering audio pipeline of
		- fatfs stream
		- mp3 decoder
		- i2s stream writer
	*/
	audio_pipeline_register(pipeline, fatfs_stream_reader, "file");
	audio_pipeline_register(pipeline, mp3_decoder, "mp3");
	audio_pipeline_register(pipeline, i2s_stream_writer, "i2s");
	
	/* Creating link tags for register to pipeline */
	const char *link tag[3] = {"file", "mp3", "i2s"};
	audio_pipeline_link(pipeline, &link_tag[0], 3);    
}

/**

	playFile(char fileName[])
	Plays file with filename from sdcard.

**/

void playFile(char fileName[]){
	/* Set uri of audio element */
	audio_element_set_uri(fatfs_stream_reader, "/sdcard/" + fileName + ".mp3");

	/* Config event handler audio interface and setting the listener */
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);
    audio_pipeline_set_listener(pipeline, evt);

	/* Setting audio event interface listener for periph */
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);

	/* Run the audio pipeline */
    audio_pipeline_run(pipeline);
}

/**

	playSentence(char sentence[8][16]);
	Plays multiple files after one, preferably words, from sdcard using playFile.

**/
void playSentence (char sentence[8][16]){
	for(int indexWord = 0; indexWord < 8; ++indexWord){
		playFile(sentence[indexWord]);
	}
}

/**

	stopPlaying();
	Breaks down audio components.

**/

void stopPlaying(){
	/* Stopping audio pipeline */
	audio_pipeline_terminate(pipeline);
	
	/* Unregistering streams */
	audio_pipeline_unregister(fatfs_stream_reader);
	audio_pipeline_unregister(pipeline, mp3_decoder);
	audio_pipeline_unregister(pipeline, i2s_stream_writer);
	
	/* Remove listeners */
	audio_pipeline_remove_listener(pipeline);
	esp_periph_set_stop_all(set);
	audio_event_iface_remove_listener(esp_periph_set_get_event_iface(set), evt);
	audio_event_iface_destroy(evt);
	
	/* Deïnitializing */
	audio_pipeline_deinit(pipeline);
	audio_element_deinit(fatfs_stream_reader);
	audio_element_deinit(i2s_stream_writer);
	audio_element_deinit(mp3_decoder);
	esp_periph_set_destroy(set);
}

/**

	maintainPlaying();
	Maintains the playing of audio, continunously call this.

**/
void maintainPlaying(){
	/*  */
	audio_event_iface_msg_t msg;
    esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK) {
			/* Potential log for esp ok */
        }

		/* Receiving music info from mp3 decoder; sample rate, bits and channel (music_info.sample_rates, music_info.bits, music_info.channels */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *) mp3_decoder
            && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO) {
            audio_element_info_t music_info = {0};
            audio_element_getinfo(mp3_decoder, &music_info);

            audio_element_setinfo(i2s_stream_writer, &music_info);
            i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
        }

        /* Stop when the last pipeline element (i2s_stream_writer in this case) receives stop event */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *) i2s_stream_writer
            && msg.cmd == AEL_MSG_CMD_REPORT_STATUS
            && (((int)msg.data == AEL_STATUS_STATE_STOPPED) || ((int)msg.data == AEL_STATUS_STATE_FINISHED))) {
            stopPlaying();
        }
}