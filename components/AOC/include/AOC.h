#ifndef AOC_H
#define AOC_H

#include <string.h>
#include <time.h>
	
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"

#define CURRENT_TIME_MODE 0
#define TIMER_MODE 1
#define STOPWATCH_MODE 2
#define ALARM_MODE 3

#define TALKING_CLOCK_ITEMS 28
#define TALKING_CLOCK_MAX_STRING 40

#define AOC_HETIS_INDEX 0
#define AOC_EN_INDEX 1
#define AOC_UUR_INDEX 2
#define AOC_MINUTEN_INDEX 3
#define AOC_1_INDEX 4
#define AOC_2_INDEX 5
#define AOC_3_INDEX 6
#define AOC_4_INDEX 7
#define AOC_5_INDEX 8
#define AOC_6_INDEX 9
#define AOC_7_INDEX 10
#define AOC_8_INDEX 11
#define AOC_9_INDEX 12
#define AOC_10_INDEX 13
#define AOC_11_INDEX 14
#define AOC_12_INDEX 15
#define AOC_13_INDEX 16
#define AOC_14_INDEX 17
#define AOC_15_INDEX 18
#define AOC_16_INDEX 19
#define AOC_17_INDEX 20
#define AOC_18_INDEX 21
#define AOC_19_INDEX 22
#define AOC_20_INDEX 23
#define AOC_30_INDEX 24
#define AOC_40_INDEX 25
#define AOC_50_INDEX 26
#define AOC_60_INDEX 27

esp_err_t AOC_fill_queue(time_t time, struct tm time_info);
void SD_init();
esp_err_t AOC_init();
void AOC_say_time(time_t time, struct tm time_info);
void AOC_task(void* pvParameter);

#endif