#include <string.h>
#include <time.h>
	
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "esp_peripherals.h"

#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "audio_error.h"
#include "fatfs_stream.h"
#include "i2s_stream.h"
#include "wav_decoder.h"
#include "board.h"
#include "periph_sdcard.h"

#include "AOC.h"

#define TAG "AUDIO_OUTPUT_CLOCK"

audio_pipeline_handle_t pipeline;
audio_element_handle_t i2s_stream_writer, wav_decoder, fatfs_stream_reader;
audio_board_handle_t board_handle;
esp_err_t ret;
QueueHandle_t AOC_queue;
int current_mode;

typedef struct {
    time_t time;
    struct tm time_info;
    int hours;
    int minutes;
    int seconds;
} time_data;

static char talking_clock_files[TALKING_CLOCK_ITEMS] [TALKING_CLOCK_MAX_STRING] = {
    "/sdcard/het_is.wav",
    "/sdcard/en.wav",
    "/sdcard/uur.wav",
    "/sdcard/minuten.wav",
    "/sdcard/een.wav",
    "/sdcard/twee.wav",
    "/sdcard/drie.wav",
    "/sdcard/vier.wav",
    "/sdcard/vijf.wav",
    "/sdcard/zes.wav",
    "/sdcard/zeven.wav",
    "/sdcard/acht.wav",
    "/sdcard/negen.wav",
    "/sdcard/tien.wav",
    "/sdcard/elf.wav",
    "/sdcard/twaalf.wav",
    "/sdcard/dertien.wav",
    "/sdcard/veertien.wav",
    "/sdcard/vijftien.wav",
    "/sdcard/zestien.wav",
    "/sdcard/zeventien.wav",
    "/sdcard/achttien.wav",
    "/sdcard/achttien.wav",
    "/sdcard/twintig.wav",
    "/sdcard/dertig.wav",
    "/sdcard/veertig.wav",
    "/sdcard/vijftig.wav",
    "/sdcard/zestig.wav"
};

void AOC_queue_send(int data){
    ret = xQueueSend(AOC_queue, &data, portMAX_DELAY);
	if (ret != ESP_OK) 
    {
		//ESP_LOGE(TAG, "Cannot queue data");
		//return ret;
	}
}

void AOC_reset_queue(){
    //reset queue
    ret = xQueueReset(AOC_queue);
	if (ret != ESP_OK){
		ESP_LOGE(TAG, "Cannot reset queue");
	}
}

esp_err_t AOC_fill_queue(time_t time, struct tm time_info){
	//  computing time
	int data = AOC_HETIS_INDEX;

	int hour = time_info.tm_hour;
	hour = (hour == 0 ? 12 : hour);
	
    if (hour < 20){
        data = AOC_1_INDEX-1+hour;
        AOC_queue_send(data);
    } else {
        if (hour % 10 != 0){
            data = AOC_1_INDEX - 1 + (hour % 10);
            AOC_queue_send(data);

            data = AOC_EN_INDEX;
            AOC_queue_send(data);
        }
        data = AOC_20_INDEX - 2 + ((hour - (hour % 10)) / 10);
        AOC_queue_send(data);
    }
    
	data = AOC_UUR_INDEX;
	AOC_queue_send(data);

    int minutes = time_info.tm_min;
    if (minutes != 0) {
        data = AOC_EN_INDEX;
        AOC_queue_send(data);

        if (minutes < 20) {
           data = AOC_1_INDEX - 1 + minutes;
           AOC_queue_send(data);
        } else {
            if (minutes % 10 != 0){
                data = AOC_1_INDEX - 1 + (minutes % 10);
                AOC_queue_send(data);

                data = AOC_EN_INDEX;
                AOC_queue_send(data);
            }
            
            data = AOC_20_INDEX - 2 + ((minutes - (minutes % 10)) / 10);
            AOC_queue_send(data);
        }

        /*if (minutes == 1){
            data = AOC_MINUUT_INDEX;
            AOC_queue_send(data);
        } else {
            data = AOC_MINUTEN_INDEX;
            AOC_queue_send(data);
        }*/
        
        data = AOC_MINUTEN_INDEX;
        AOC_queue_send(data);
    }
    ESP_LOGI(TAG, "Queue filled with %d items", uxQueueMessagesWaiting(AOC_queue));
	return ESP_OK;
}

void SD_init(){
    esp_log_level_set("*", ESP_LOG_WARN);
	esp_log_level_set(TAG, ESP_LOG_INFO);
	
	ESP_LOGI(TAG, "Init sdcard");
	esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
	esp_periph_set_handle_t set = esp_periph_set_init(&periph_cfg);
	audio_board_sdcard_init(set, SD_MODE_1_LINE);
	
	ESP_LOGI(TAG, "Init codec");
	board_handle = audio_board_init();
	audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);
}

esp_err_t AOC_init(){
	// Initialize queue
	ESP_LOGI(TAG, "Creating FreeRTOS queue for talking clock");

    AOC_queue = xQueueCreate( 10, sizeof( int ) );
	
	if (AOC_queue == NULL) 
    {
		ESP_LOGE(TAG, "Error creating queue");
		return ESP_FAIL;
    }
    
    xTaskCreate(AOC_task, "AOC_task", 1024 * 10, NULL, 2, NULL);
    ESP_LOGI(TAG, "End of AOC init");
	return ESP_OK;
}

void AOC_say_time(time_t time, struct tm time_info){    
	ESP_LOGI(TAG, "play time");

	audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
    switch (el_state) 
    {
        case AEL_STATE_INIT:
            ESP_LOGI(TAG, "[ * ] Starting audio pipeline");
            AOC_reset_queue();
            ESP_LOGI(TAG, "Queue reset");
            AOC_fill_queue(time, time_info);
            ESP_LOGI(TAG, "Queue filled");
            audio_element_set_uri(fatfs_stream_reader, talking_clock_files[AOC_HETIS_INDEX]);
            ESP_LOGI(TAG, "uri set");
            audio_pipeline_run(pipeline);
            ESP_LOGI(TAG, "pipeline runned");
            break;
        case AEL_STATE_RUNNING:
            ESP_LOGI(TAG, "[ * ] Pausing audio pipeline");
            audio_pipeline_pause(pipeline);
            // Clear Queue
            //xQueueReset(AOC_queue);
            break;
        case AEL_STATE_PAUSED:
            ESP_LOGI(TAG, "[ * ] Resuming audio pipeline");
            // Create new queue
            // Set first item in the queue
            AOC_reset_queue();
            AOC_fill_queue(time, time_info);
            audio_element_set_uri(fatfs_stream_reader, talking_clock_files[AOC_HETIS_INDEX]); // Set first sample
            audio_pipeline_reset_ringbuffer(pipeline);
            audio_pipeline_reset_elements(pipeline);
            audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
            audio_pipeline_run(pipeline);
            break;
        default:
            ESP_LOGI(TAG, "[ * ] Not supported state %d", el_state);
    }
}


void AOC_task(void* pvParameter){
	ESP_LOGI(TAG, "[3.0] Create audio pipeline for playback");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    mem_assert(pipeline);

    ESP_LOGI(TAG, "[3.1] Create fatfs stream to read data from sdcard");
    char *url = NULL;
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);
    audio_element_set_uri(fatfs_stream_reader, url);

    ESP_LOGI(TAG, "[3.2] Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);

    ESP_LOGI(TAG, "[3.3] Create wav decoder to decode wav file");
    wav_decoder_cfg_t wav_cfg = DEFAULT_WAV_DECODER_CONFIG();
    wav_decoder = wav_decoder_init(&wav_cfg);

    ESP_LOGI(TAG, "[3.4] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, fatfs_stream_reader, "file");
    audio_pipeline_register(pipeline, wav_decoder, "wav");
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2s");

    ESP_LOGI(TAG, "[3.5] Link it together [sdcard]-->fatfs_stream-->wav_decoder-->i2s_stream-->[codec_chip]");
    audio_pipeline_link(pipeline, (const char *[]) {"file", "wav", "i2s"}, 3);

    audio_element_set_uri(fatfs_stream_reader, talking_clock_files[AOC_HETIS_INDEX]); //intro file

    ESP_LOGI(TAG, "[ 4 ] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    audio_event_iface_handle_t evt = audio_event_iface_init(&evt_cfg);

    ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(pipeline, evt);

    ESP_LOGI(TAG, "[ 5 ] Start audio_pipeline");
    audio_pipeline_run(pipeline);

    ESP_LOGI(TAG, "[ 6 ] Listen for all pipeline events");
    while (1) 
    {
        /* Handle event interface messages from pipeline
           to set music info and to advance to the next song
        */
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK) 
        {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT) 
        {
            // Set music info for a new song to be played
            if (msg.source == (void *) wav_decoder
                && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO) 
                {
                audio_element_info_t music_info = {};
                audio_element_getinfo(wav_decoder, &music_info);
                ESP_LOGI(TAG, "[ * ] Received music info from wav decoder, sample_rates=%d, bits=%d, ch=%d",
                         music_info.sample_rates, music_info.bits, music_info.channels);
                audio_element_setinfo(i2s_stream_writer, &music_info);
                continue;
            }
            // Advance to the next song when previous finishes
            if (msg.source == (void *) i2s_stream_writer
                && msg.cmd == AEL_MSG_CMD_REPORT_STATUS) 
                {
                audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
                if (el_state == AEL_STATE_FINISHED) 
                {
                    int element = 0;
					if (uxQueueMessagesWaiting(AOC_queue) > 0 && 
						xQueueReceive(AOC_queue, &element, portMAX_DELAY)) {
						ESP_LOGI(TAG, "Finish sample, towards next sample");

                        url = talking_clock_files[element];

						ESP_LOGI(TAG, "URL: %s", url);
						audio_element_set_uri(fatfs_stream_reader, url);
						audio_pipeline_reset_ringbuffer(pipeline);
						audio_pipeline_reset_elements(pipeline);
						audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
						audio_pipeline_run(pipeline);
					} 
                    else
                    {
						// No more samples. Pause for now
                        ESP_LOGI(TAG, "pause pipeline!");
						audio_pipeline_pause(pipeline);
					}
                }
                continue;
            }  
        }
    }

    ESP_LOGI(TAG, "[ 7 ] Stop audio_pipeline");
    audio_pipeline_terminate(pipeline);

    ESP_LOGI(TAG, "unregister");
    audio_pipeline_unregister(pipeline, fatfs_stream_reader);
    audio_pipeline_unregister(pipeline, i2s_stream_writer);
    audio_pipeline_unregister(pipeline, wav_decoder);

    ESP_LOGI(TAG, "Remove Listener");
    /* Terminal the pipeline before removing the listener */
    audio_pipeline_remove_listener(pipeline);

    ESP_LOGI(TAG, "destroy");
    /* Make sure audio_pipeline_remove_listener & audio_event_iface_remove_listener are called before destroying event_iface */
    audio_event_iface_destroy(evt);

    ESP_LOGI(TAG, "DEINIT");
    /* Release all resources */
    audio_pipeline_deinit(pipeline);
    audio_element_deinit(fatfs_stream_reader);
    audio_element_deinit(i2s_stream_writer);
    audio_element_deinit(wav_decoder);

    ESP_LOGI(TAG, "End of task");

    vTaskDelete(NULL);
}