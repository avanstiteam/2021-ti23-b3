#include "clocksync.h"
#include "esp_peripherals.h"
#include "esp_http_client.h"
#include "periph_wifi.h"
#include "sntp_sync.h"
#include <sys/time.h>

/*
    Contains methods needed for time synchronisation
*/


/*
Callback when an stmp timesynchronisation is triggerd.
*/
void stmp_timesync_event(struct timeval *tv)
{
    ESP_LOGI("CLOCK", "Notification of a time synchronization event");
	
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI("CLOCK", "The current date/time in Amsterdam is: %s", strftime_buf);
	
}

/*
Sets up the wifi and callbackmethods for the time and clock synchronisation
*/
void clock_init(){
    //setup wifi
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    esp_periph_set_handle_t set = esp_periph_set_init(&periph_cfg);
    periph_wifi_cfg_t wifi_cfg = {
        .ssid = CONFIG_WIFI_SSID,
        .password = CONFIG_WIFI_PASSWORD,
    };

    ESP_LOGI("CLOCK_INIT", "get wifi handle");
    esp_periph_handle_t wifi_handle = periph_wifi_init(&wifi_cfg);
    ESP_LOGI("CLOCK_INIT", "Starting peripheral");
    esp_periph_start(set, wifi_handle);
    ESP_LOGI("CLOCK_INIT", "wifi wait for connected");
    periph_wifi_wait_for_connected(wifi_handle, portMAX_DELAY);

       // Synchronize NTP time
    ESP_LOGI("CLOCK_INIT", "setting callback");
	sntp_sync(stmp_timesync_event);
    
    
}