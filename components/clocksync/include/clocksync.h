#ifndef CLOCKSYNC_H
#define CLOCKSYNC_H

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "sdkconfig.h"
#include "string.h"
#include <sys/time.h>

#if __has_include("esp_idf_version.h")
#include "esp_idf_version.h"
#else
#define ESP_IDF_VERSION_VAL(major, minor, patch) 1
#endif

#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 1, 0))
#include "esp_netif.h"
#else
#include "tcpip_adapter.h"
#endif

/*
    Contains methods needed for time synchronisation
*/

/*
Sets up the wifi and callbackmethods for the time and clock synchronisation
*/
void clock_init();

/*
Callback when an stmp timesynchronisation is triggerd.
*/
void stmp_timesync_event(struct timeval *tv);

#endif

