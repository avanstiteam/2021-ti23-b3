#ifndef HK_VOLUME_H
#define HK_VOLUME_H

#if __has_include("esp_idf_version.h")
#include "esp_idf_version.h"
#else
#define ESP_IDF_VERSION_VAL(major, minor, patch) 1
#endif
/*
Conrtols the audio
*/

/*
Initialises the audio control
*/
void init_volume();

/*
Gets the current active volume
return int:		The current volume, from 0 - 100
*/
int get_volume();

/*
Sets the volume
int to_volume:	the volume from 0 - 100
return int:		the current volume from 0 - 100
*/
int set_volume(int to_volume);

/*
Adds an amount to the volume. The volume maximum is 100
int amount:  the amount to be added to the volume
return int:	 returns the current volume from 0 - 100
*/
int volume_up(int amount);

/*
Subtracts an amount to the volume. The volume minimum is 0
int amount:  the amount to be added to the volume
return int:	 returns the current volume from 0 - 100
*/
int volume_down(int amount);

#endif