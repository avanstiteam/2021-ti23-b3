#include "volume_module.h"

#include "audio_common.h"

#include "esp_peripherals.h"
#include "board.h"

#define TAG VOLUME

audio_board_handle_t board_handle;
int volume_level;

/*
Initialises the audio control
*/
void init_volume(){
	board_handle = audio_board_init();
	
	audio_hal_get_volume(board_handle->audio_hal, &volume_level);
}

/*
Sets the volume maximum to 100 and minimum to 0
int volume_to_validate:	The volume to be potentially adjusted
return int:				The volume between 0 and 100
*/
int volume_cap(int volume_to_validate){
	if (volume_to_validate > 100){
		return 100;
	} else if (volume_to_validate < 0){
		return 0;
	}
	return volume_to_validate;
}

/*
Gets the current active volume
return int:		The current volume, from 0 - 100
*/
int get_volume(){
	return volume_level;
}

/*
Sets the volume
int to_volume:	the volume from 0 - 100
return int:		the current volume from 0 - 100
*/
int set_volume(int to_volume){
	to_volume = volume_cap(to_volume);
	audio_hal_set_volume(board_handle->audio_hal, to_volume);
	volume_level = to_volume;
	return volume_level;
}

/*
Adds an amount to the volume. The volume maximum is 100
int amount:  the amount to be added to the volume
return int:	 returns the current volume from 0 - 100
*/
int volume_up(int amount){
	return set_volume((volume_level + amount));
}

/*
Subtracts an amount to the volume. The volume minimum is 0
int amount:  the amount to be added to the volume
return int:	 returns the current volume from 0 - 100
*/
int volume_down(int amount){
	return set_volume((volume_level - amount));
}