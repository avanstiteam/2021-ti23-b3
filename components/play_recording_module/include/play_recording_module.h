#ifndef PLAY_RECORDING_MODULE_H
#define PLAY_RECORDING_MODULE_H

#define MAX_RECORDING_AMOUNT 10
#define MAX_RECORDING_STRING_LENGTH 15

/*
The module responsible for recording and playing audio files
*/

/*
A list of predetermined audio paths.
*/
static char recording_files[MAX_RECORDING_AMOUNT] [MAX_RECORDING_STRING_LENGTH] = {
    "recEen.wav",
    "recTwee.wav",
    "recDrie.wav",
    "recVier.wav",
    "recVijf.wav",
    "recZes.wav",
    "recZeven.wav",
    "recAcht.wav",
    "recNegen.wav",
    "recTien.wav",
};

/*
Plays a recording
char recordingName[20]:     the path of the file
*/
void play_recording(char recordingName[20]);

/*
Records a 15 recording and saves it. Overwrites if the filepath already exists.
char recording_name[20]:    the path of the recording where it will be saved
*/
void record_recording(char recordingName[20]);

/*
Initialises the SD-card
*/
void init_SD();

#endif