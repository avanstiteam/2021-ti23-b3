#ifndef HK_RADIO_H
#define HK_RADIO_H

#include <string.h>
#include <time.h>
	
#include <string.h>

#if __has_include("esp_idf_version.h")
#include "esp_idf_version.h"
#else
#define ESP_IDF_VERSION_VAL(major, minor, patch) 1
#endif

#if (ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(4, 1, 0))
#include "esp_netif.h"
#else
#include "tcpip_adapter.h"
#endif

QueueHandle_t radio_queue;
TaskHandle_t radio_task_handle;

#define HKR_task_tag "HK_radio_task_handler"

#define RADIO_ITEMS 6
#define RADIO_NAME_MAX_STRING 24
#define RADIO_URL_MAX_STRING 128

#define QUEUE_PAUSE 100
#define QUEUE_DEINIT 101

// Radio constants
#define RADIO_RADIO1 0
#define RADIO_3FM 1
#define RADIO_538 2
#define RADIO_538_NONSTOP 3
#define RADIO_QMUSIC 4
#define RADIO_BNR 5

typedef struct RADIO {
	char url[RADIO_URL_MAX_STRING];
	char name[RADIO_NAME_MAX_STRING];
} RADIO;

static RADIO radio_list[RADIO_ITEMS] = {
	{"https://icecast.omroep.nl/radio1-bb-aac", "Radio 1"},
	{"https://icecast.omroep.nl/3fm-bb-aac", "Radio 3FM"},
	{"https://icecast.omroep.nl/3fm-alternative-aac", "Radio 538"},
	{"http://playerservices.streamtheworld.com/api/livestream-redirect/TLPSTR09AAC.aac", "Radio 538 Non-stop"},
	{"https://icecast-qmusicnl-cdp.triple-it.nl/Qmusic_nl_live_32.aac", "Q-music"},
	{"http://stream.bnr.nl/bnr_aac_96_20", "BNR Nieuwsradio"}	
};

/*
static char radio_url_list[RADIO_URL_ITEMS][RADIO_URL_MAX_STRING] = {
	"https://icecast.omroep.nl/radio1-bb-aac", 												// Radio 1
	"https://icecast.omroep.nl/3fm-bb-aac",													// 3FM
	"https://icecast.omroep.nl/3fm-alternative-aac",										// 3FM Alternative
	"http://playerservices.streamtheworld.com/api/livestream-redirect/RADIO538AAC.aac",		// Radio 538
	"http://playerservices.streamtheworld.com/api/livestream-redirect/TLPSTR09AAC.aac",		// Radio 538 Non-stop
	"https://icecast-qmusicnl-cdp.triple-it.nl/Qmusic_nl_live_32.aac",						// Q-music
	"http://stream.bnr.nl/bnr_aac_96_20"													// BNR
};
*/

void HK_radio_task(void* pvParameters);

/**
	Initializes HK radio.
	First use this function.
**/
esp_err_t HK_radio_init(int HK_radio_index);

/**
	Sets radio using an constant of a radio (RADIO_XXX), see above
**/
void HK_radio_set(int HK_radio_index);

/**
	Deinit radio
**/
void HK_radio_deinit();

#endif